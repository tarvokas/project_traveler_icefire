﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;

namespace Project_ReisivadTöötajad.Models
{
    partial class Person
    {
        public string FullName => FirstName + " " + LastName;

        public static Person GetByEmail(string email)
        {
            //static 
                TagaridaEntities db = new TagaridaEntities();
            return db.People.Where(x => x.Email == email).SingleOrDefault();
        }

        public bool IsInRole(string rolename)
            => this.UserInRoles
                .Select(x => x.Role.RoleName.ToLower()).ToList()
                .Intersect(rolename.Split(',').Select(y => y.Trim().ToLower()))
                .Count() > 0;
    }
}

namespace Project_ReisivadTöötajad.Controllers  
{
    public class PersonController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: Person
        public ActionResult Index()
        {
            var people = db.People.Include(p => p.Department).Include(p => p.File).OrderBy(t => t.StartDate).ToList();
            return View(people.ToList());
        }

        public ActionResult AddRole(int? id, int? roleid)         
        {
            if (id == null)             
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);             
            if (person == null)                             
            {
                return HttpNotFound();
            }

            if (roleid == null)         
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Role role = db.Roles.Find(roleid);              
            if (role == null)                               
            {
                return HttpNotFound();
            }

            try
            {
                db.UserInRoles.Add(new UserInRole { PersonId = id.Value, RoleId = roleid.Value });   
                db.SaveChanges();
            }
            catch { }

            return RedirectToAction("Details", new { id = id });
        }

        public ActionResult RemoveRole(int? id, int? roleid)
        {
            if (id == null)         
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);      
            if (person == null)                      
            {
                return HttpNotFound();
            }

            if (roleid == null)         
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userinrole = person.UserInRoles.Where(x => x.RoleId == roleid).FirstOrDefault();        
            if (userinrole != null)
            {
                db.UserInRoles.Remove(userinrole);          
                db.SaveChanges();
            }
            return RedirectToAction("Details", new { id = id });
        }

        // GET: Person/Details/5

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.OtherRoles = db
                .Roles
                .Where(x => !x.UserInRoles.Select(y => y.PersonId).Contains(id.Value)).ToList();
            return View(person);
        }


        // GET: Person/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName");
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename");
            return View();
        }

        // POST: Person/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,DepartmentID,Email")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", person.Picture);
            return View(person);
        }

        // GET: Person/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", person.Picture);
            return View(person);
        }

        // POST: Person/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,PersonState,DepartmentID,Picture,Email")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", person.Picture);
            return View(person);
        }

        // GET: Person/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: Person/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
