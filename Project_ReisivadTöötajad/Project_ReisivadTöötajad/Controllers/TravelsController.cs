﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;
using PagedList;


namespace Project_ReisivadTöötajad.Models
{

    partial class Travel
    {
        public static Dictionary<int, string> ReportStates = new Dictionary<int, string>
        {
            {0, "Created"},
            {1, "Sent For Approval"},
            {2, "Declined"},
            {3, "Approved"},
            {4, "Paid Out" }

        };
        public string _ReportState =>
            ReportStates.ContainsKey(ReportState) ? ReportStates[this.ReportState] : "";

        public int Duration => StartDate.HasValue && EndDate.HasValue ? ((EndDate.Value - StartDate.Value).Days) + 1 : 0;
        public int NightsDuration => StartDate.HasValue && EndDate.HasValue ? (EndDate.Value - StartDate.Value).Days : 0;

        int DaysOver => Duration - 15;
        public int DailyAllowance => Duration > 15 ? (15 * 50) + (DaysOver * 32) : Duration * 50;
    }
}

namespace Project_ReisivadTöötajad.Controllers
{
    public class TravelsController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: Travels
        public ActionResult Index(string sortOrder, int? currentFilter, string searchString, int? page, int? personId)   
                                                                                                                         
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName"); 
            ViewBag.ReportState = new SelectList(Travel.ReportStates, "Key", "Value");

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";


            if (personId > 0)
            {
                page = 1;
            }
            else
            {
                personId = currentFilter;
            }
            ViewBag.CurrentFilter = personId;

            var travels = from s in db.Travels.Include(t => t.Person)
                          select s;

            if (personId > 0)
            {
                travels = travels.Where(v => v.PersonId == personId);
            }

            switch (sortOrder)
            {
                case "name_desc":
                    travels = travels.OrderByDescending(s => s.Person.FirstName);
                    break;
                case "Date":
                    travels = travels.OrderBy(s => s.StartDate);
                    break;
                case "date_desc":
                    travels = travels.OrderByDescending(s => s.StartDate);
                    break;
                default:
                    travels = travels.OrderBy(s => s.Person.FirstName);
                    break;
            }
            int pageSize = 5; //siin saab muuta, mitu kirjet ühel lehel kuvatakse.
            int pageNumber = (page ?? 1);

            return View(travels.ToPagedList(pageNumber, pageSize));
        }

        // GET: Travels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }
            return View(travel);
        }

        // GET: Travels/Create
        public ActionResult Create()
        {
            //TODO: Uut avaldust peaks saama esitada töötaja ja raamatupidaja õigustega isik vaid enda nimel. Admin ja direktor nii enda kui ka teiste nimel.
            //TODO: Osakonnajuhil peaks rippmenüüs olema nii tema ja kui ka tema osakonnaliikmete nimed.
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
            ViewBag.ReportState = new SelectList(Travel.ReportStates, "Key", "Value");

            return View();
        }

        // POST: Travels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create([Bind(Include = "Id,StartDate,EndDate,Description,ReportState,PersonId,TravelDestination,TripPurpose,TravelName")] Travel travel)

        {
            if (ModelState.IsValid)
            {
                db.Travels.Add(travel);
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = travel.Id });
            }

            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName", travel.PersonId);
            ViewBag.ReportState = new SelectList(Travel.ReportStates, "Key", "Value");
            return View(travel);


        }

        // GET: Travels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }

            //TODO: Vastavalt rollile vaja muuta listi kuvatavate isikute nimesid
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName", travel.PersonId);
            ViewBag.ReportState = new SelectList(Travel.ReportStates, "Key", "Value");

            return View(travel);
        }

        // POST: Travels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, StartDate, EndDate, Description, ReportState, PersonId, TravelDestination, TripPurpose, TravelName")] Travel travel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(travel).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName", travel.PersonId);
            ViewBag.ReportState = new SelectList(Travel.ReportStates, "Key", "Value");
            return View(travel);
        }

        // GET: Travels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReportState = new SelectList(Travel.ReportStates, "Key", "Value");
            return View(travel);
        }

        // POST: Travels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Travel travel = db.Travels.Find(id);
            db.Travels.Remove(travel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
