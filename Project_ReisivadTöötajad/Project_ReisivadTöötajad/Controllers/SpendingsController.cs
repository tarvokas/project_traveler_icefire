﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;

namespace Project_ReisivadTöötajad.Controllers
{
    public class SpendingsController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: Spendings
        public ActionResult Index(int? travelid)
        {
            var spendings = db.Spendings
                .Include(s => s.File)
                .Include(s => s.Travel)
                .Include(s => s.SpendingType)
                .Where(t=> t.TravelId == travelid)
                ;

            if (travelid == null)         
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.TravelId = travelid;     
            return View(spendings.ToList());
        }


        public ActionResult RemoveFile(int? spendingid, int? fileid)  
        {
            if (spendingid == null)         
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(spendingid);      
            if (spending == null)                      
            {
                return HttpNotFound();
            }

            if (fileid == null)         
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var spendingfile = spending.SpendingFiles.Where(x => x.FileId == fileid).FirstOrDefault();        
            if (spendingfile != null)
            {
                db.SpendingFiles.Remove(spendingfile);          
                db.SaveChanges();
            }

            File deletefile = db.Files.Find(fileid);      
            db.Files.Remove(deletefile);

            return RedirectToAction("Edit", new { id = spendingid });   
        }



        // GET: Spendings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
            return View(spending);
        }

        // GET: Spendings/Create
        public ActionResult Create(int travelid )
        {
            //if (spendingid == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            Spending spending = new Spending { TravelId = travelid };

            ViewBag.Document = new SelectList(db.Files, "Id", "Filename");
            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName");
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
            return View(spending);
        }

        // POST: Spendings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TravelId,TypeId,Amount,ServiceProvider,StartDate,EndDate,VatEUR,PaidOut")] Spending spending)
        {
            if (ModelState.IsValid)
            {
                db.Spendings.Add(spending);
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = spending.Id });
            }

            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName", spending.TravelId);
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);
            return View(spending);
        }

        // GET: Spendings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);
            return View(spending);
        }

        // POST: Spendings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit([Bind(Include = "Id,TravelId,TypeId,Amount,ServiceProvider,StartDate,EndDate,VatEUR,PaidOut")] Spending spending, HttpPostedFileBase file) // lisatud , HttpPostedFileBase file
        {
            if (ModelState.IsValid)
            {
                db.Entry(spending).State = EntityState.Modified;
                db.SaveChanges();

                //lisatud
                if (file != null && file.ContentLength > 0)
                {
                    using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                    {                       
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        File f = new File { Content = buff, ContentType = file.ContentType, Filename = file.FileName.Split('\\').Last().Split('/').Last() };
                        db.Files.Add(f);
                        db.SaveChanges();
                        spending.Document = f.Id;
                        db.SaveChanges();

                        db.SpendingFiles.Add(new SpendingFile { SpendingId = spending.Id, FileId = f.Id });   
                        db.SaveChanges();
                    }
                } 


               return RedirectToAction("Edit");
            }
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);

            return View(spending);

        }

        // GET: Spendings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
            return View(spending);
        }

        // POST: Spendings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Spending spending = db.Spendings.Find(id);
            db.Spendings.Remove(spending);
            db.SaveChanges();
            return RedirectToAction("Edit","Travels", new { id = spending.TravelId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
